X. A readme with:   Your thoughts about the code. What makes it amazing code. Or what makes it ok code. Or what makes it terrible code. How would you have done it. Thoughts on formatting, structure, logic.. The more details that you can provide about the code (what's terrible about it or/and what is good about it) the easier for us to assess your coding style, mentality etc
==================================================================================================================================================================================================

In my evaluation, I find this code to be of poor quality. While the directory structure seems acceptable, the core logic is placed within a repository. If I were to undertake this, I would strive to standardize the codebase, creating repositories with clear, singular purposes. My attempt at refactoring would involve renaming all methods, appending "updated" to their names.

Notable issues include:

1- Excessive method length, leading to complexity.
2- Code is disorganized, making comprehension difficult.
3- Queries are invoked within loops, leading to inefficiency.
4- Presence of superfluous, unnecessary code segments.

These are just a few concerns among others that need to be addressed.

==================================================================================================================================================================================================
Y.  Refactor it if you feel it needs refactoring. The more love you put into it. The easier for us to asses your thoughts, code principles etc

My answer:
I endeavored to refactor the code. Kindly review it and provide your assessment. My primary focus was on restructuring the logic present in the initial method.
==================================================================================================================================================================================================
